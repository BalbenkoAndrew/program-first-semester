from SolarSystem import *
from Sun import *
from Planet import *

def main():
   ss = SolarSystem(2,2)

   sun = Sun("SUN", 5000, 10, 5800)
   ss.addSun(sun)

   m = Planet("MERCURY", 19.5, 1000, .25, 0, 2, "blue")
   ss.addPlanet(m)

   m = Planet("EARTH", 47.5, 5000, 0.3, 0, 2.0, "green")
   ss.addPlanet(m)

   m = Planet("MARS", 50, 9000, 0.5, 0, 1.63, "red")
   ss.addPlanet(m)

   m = Planet("Pluto", 1, 500, 0.9, 0, .5, "orange")
   ss.addPlanet(m)

   m = Planet("Asteroid", 1, 500, 1.0, 0, .75, "cyan")
   ss.addPlanet(m)

   numTimePeriods = 20000
   for amove in range(numTimePeriods):
        ss.movePlanets()

   ss.freeze()

main()