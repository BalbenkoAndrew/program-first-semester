import turtle
import math

class Sun:
   def __init__(self, iname, irad, im, itemp):
       self.name = iname
       self.radius = irad
       self.mass = im
       self.temp = itemp
       self.x = 0
       self.y = 0
       self.sturtle = turtle.Turtle()
       self.sturtle.shape("circle")  #форма и цвет солнца
       self.sturtle.color("yellow")


   def getName(self):
       return self.name

   def getRadius(self):
       return self.radius

   def getMass(self):
       return self.mass

   def getTemperature(self):
       return self.temp

   def getVolume(self):
       v = 4.0/3 * math.pi * self.radius**3
       return v

   def getSurfaceArea(self):
       sa = 4.0 * math.pi * self.radius**2
       return sa

   def getDensity(self):
       d = self.mass / self.getVolume()
       return d

   def setName(self, newname):
       self.name = newname

   def __str__(self):
       return self.name

   def getXPos(self):
       return self.x

   def getYPos(self):
       return self.y