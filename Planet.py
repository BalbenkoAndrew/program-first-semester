import turtle
import math

class Planet:

   def __init__(self, iname, irad, im, idist, ivx, ivy, ic):
       self.name = iname
       self.radius = irad
       self.mass = im
       self.distance = idist
       self.x = idist
       self.y = 0
       self.velx = ivx
       self.vely = ivy
       self.color = ic

       self.pturtle = turtle.Turtle()
       self.pturtle.up() #линия от солнца, не рисовать
       self.pturtle.color(self.color) #цвет планеты
       self.pturtle.shape("circle") #форма планеты
       self.pturtle.goto(self.x,self.y) #координаты какие-то
       self.pturtle.pu() #не рисовать орбиту

   def getName(self):
       return self.name

   def getRadius(self):
       return self.radius

   def getMass(self):
       return self.mass

   def getDistance(self):
       return self.distance

   def getVolume(self):
       v = 4.0/3 * math.pi * self.radius**3
       return v

   def getSurfaceArea(self):
       sa = 4.0 * math.pi * self.radius**2
       return sa

   def getDensity(self):
       d = self.mass / self.getVolume()
       return d

   def setName(self, newname):
       self.name = newname

   def show(self):
        print(self.name)

   def __str__(self):
       return self.name

   def moveTo(self, newx, newy):
       self.x = newx
       self.y = newy
       self.pturtle.goto(newx, newy)

   def getXPos(self):
       return self.x

   def getYPos(self):
       return self.y

   def getXVel(self):
       return self.velx

   def getYVel(self):
       return self.vely

   def setXVel(self, newvx):
       self.velx = newvx

   def setYVel(self, newvy):
       self.vely = newvy